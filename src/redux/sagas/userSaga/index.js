import { takeEvery, put, call} from "@redux-saga/core/effects";
import { startRequest,endRequest, startRequestTrack,endRequestTrack } from "../../reducer/albums";
import fetchData from "../../../api";


function* requstData() {
  let data = yield call(fetchData, "/v1/me/playlists");
  yield put(endRequest(data))
}

function* requestTrack({payload}){
  const url = payload.url.split(".com")[1];
  const track = yield call(fetchData,url);
  yield put(endRequestTrack(track));
}

export default function* watcherUser() {
  yield takeEvery(startRequest.type, requstData);
  yield takeEvery(startRequestTrack.type,requestTrack)
}
