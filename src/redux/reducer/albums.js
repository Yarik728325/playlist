import { createSlice } from "@reduxjs/toolkit";


export const check = createSlice({
  name:"toolkit",
  initialState:{
    tmp:0,
    PlayList:null,
    TracksinSomePlayList:{
      img:null,
      data:null
    }
  },
  reducers:{
    addSome(state){
      state.tmp++;
    },
    startRequest(state){
      state.loading = true;
    },
    endRequest(state,actions){
      state.loading = false
      state.PlayList = actions.payload
    },
    startRequestTrack(state,{payload}){
      state.loading = true 
      state.TracksinSomePlayList.img = payload.img;
    },
    endRequestTrack(state,{payload}){
      state.loading = false
      state.TracksinSomePlayList.data = payload
    }
  }
})

export default check.reducer;

export const { startRequest,endRequest,addSome,startRequestTrack,endRequestTrack} = check.actions;
