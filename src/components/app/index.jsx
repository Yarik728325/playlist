import React from "react";
import Routing from "../../routes";
import Navigation from "../Navigation";
import LeftBar from "../LeftBar";
import './style.scss';

const App =  ()=>{
  return (
    <>
      <div className="main_wrapper">
        <header>
          <Navigation/>
        </header>
        <main id="Items">
        <LeftBar />
          <Routing/>
        </main>
      </div>
    </>
  )
}

export default App;