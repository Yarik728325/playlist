import React,{ useState, useRef } from "react";

const Track = ({name, album, preview_url})=>{
  const [isPlaying,setPlaying] = useState(false);
  const audio = useRef();
  const toggleAudio = (event) => {
    if (isPlaying) {
      audio.current.pause();
      setPlaying(prevState=>!prevState)
    } else {
      audio.current.play();
      setPlaying(prevState=>!prevState)
    }
  }
  return(
    <div className="track">
      <div className="left">
        <img src={album.images[0].url} alt="" />
        <div className="track_name">{name}</div>
      </div>
      <div className="right">
      <div className="container" onClick={()=>toggleAudio()} >
      <audio preload="metadata" controls controlsList="nodownload" ref={audio} >
        <source src={ preview_url } type="audio/mpeg"/>
      </audio>
      <div className={`play-button ${isPlaying?"playing":""}`}> </div>
    </div>
    </div>
  </div>
  )
}

export default Track;