import React, { useEffect, memo } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { startRequest } from "../../redux/reducer/albums";
import Spinner from "../Spinner";
import './style.scss';

const LeftBar = ()=>{
  const { PlayList  } = useSelector(state=>state.albums);
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(startRequest());
  },[dispatch])
  if( !PlayList){
    return (
      <div className="leftBar">
        <Spinner/>
      </div>
    )
  }
  return(
    <div className="leftBar">
        <ul>
          {
            PlayList.map(e=>{
              const { images,name } = e;
              return(
                <li key={(Math.random() + 1).toString(36).substring(7)} >  <img src={images[0].url}alt="" /><div className="text">{name}</div></li>
              )
            })
          }
        </ul>
    </div>
  )
}

export default memo(LeftBar);