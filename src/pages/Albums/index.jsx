import React, { useEffect } from "react";
import { startRequest } from "../../redux/reducer/albums";
import { useDispatch, useSelector } from "react-redux";
import CardTable from "../../components/PlayListItems";

const Albums = ()=>{
  const dispatch = useDispatch();
  const { loading,dataAlbum } = useSelector(state=>state.albums);
  useEffect(()=>{
    dispatch(startRequest('albom'));
  },[dispatch])
  if(!dataAlbum){
    return <h2>Loading...</h2>
  }
  const {items} = dataAlbum;
  return (
    <div className="Albom_wrapper">
      {
        items.map(e=>{
          const {name,owner} = e; 
          console.log(name,owner);
          return <CardTable name={name} owner={owner} />
        })
      }
    </div>
  )
}

export default Albums;