import React, { useRef, useState } from "react";
import { useSelector } from "react-redux";
import Spinner from "../../components/Spinner";
import './style.scss';
import Track from "../../components/Track";

export default function DeteilItem() {
  const { loading,TracksinSomePlayList } = useSelector(state=>state.albums);
  if(loading){
    return <Spinner/>
  }
  console.log(TracksinSomePlayList);
  return (
   <div className="Deteil_item">
     <img src={TracksinSomePlayList.img} alt="" />
     <div className="deteil_wrapper">
       {
         TracksinSomePlayList.data.map(e=>{
           const { name,album, preview_url } = e.track;
           return(
             <Track
              name={name}
              album={album}
              preview_url={preview_url}
              key={(Math.random() + 1).toString(36).substring(7)}
             />
            //  <div className="track" key={(Math.random() + 1).toString(36).substring(7)}>
            //    <div className="left">
            //      <img src={album.images[0].url} alt="" />
            //       <div className="track_name">{name}</div>
            //    </div>
            //    <div className="right">
            //    <div class="container" onClick={()=>toggleAudio()} >
            //     <audio preload="metadata" controls controlslist="nodownload" ref={audio} >
            //       <source src="https://www.buzzsprout.com/231452/3483301-magic-mastering-jj-from-goodpods-stops-by.mp3" type="audio/mpeg"/>
            //     </audio>
            //     <div class={`play-button ${isPlaying?"":"playing"}`}> </div>
            //   </div>
            //    </div>
            //  </div>
           )
         })
       }
     </div>
   </div>
  )
}
