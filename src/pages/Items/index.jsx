import React, {useEffect}  from "react";
import { useDispatch, useSelector } from 'react-redux';
import { startRequest, startRequestTrack } from "../../redux/reducer/albums";
import Spinner from "../../components/Spinner";
import './style.scss';
import { Link } from "react-router-dom";

const Items = ()=>{
  const { loading, PlayList  } = useSelector(state=>state.albums);
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(startRequest());
  },[dispatch])
  const changeTracks = ({url,img})=>{
    const data = {
      img,
      url
    }
    dispatch(startRequestTrack(data));
  }
  if(loading || !PlayList){
    return <Spinner/>
  }
  return (
    <> 
    <div className="Items_wrapper" >
     {
       PlayList.map((e,index)=>{
         const { images,name,tracks } = e;
         if(index === 0){
           return(
            <Link 
            to={`/playList/${index}`} 
            className="styled  first" 
            key={(Math.random() + 1).toString(36).substring(7)}
            onClick={ ()=>changeTracks({
              url:tracks.href,
              img:images[0].url
            })}
            >
              <div className="hover">{name}</div>
              <img src={images[0].url} alt="" />
            </Link>
           ) 
         }
         return (
           <Link 
           to={`/playList/${index}`} 
           className="styled other" 
           key={(Math.random() + 1).toString(36).substring(7)}
           onClick={ ()=>changeTracks({
             url:tracks.href,
             img:images[0].url
           })}
           >
              <div className="hover">{name}</div>
              <img src={images[0].url} alt="" />
           </Link>
         )
       })
     }
    </div>
    </>
  );
}

export default Items;