import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";
import Items from "../pages/Items";
import DeteilItem from "../pages/DeteilsItem";


const Routing = () =>{
  return(
    <Routes>
      {/* <Route path="/"  element={<Home/>} /> */}
      <Route path='/' element={<Items/>} />
      <Route path='/playList/:id' element={<DeteilItem/>} />
      <Route path="*" element={<NotFound/>} />
    </Routes>
  )
}

export default Routing;